import React, { useState } from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';

const Example = (props) => {
  const [collapsed, setCollapsed] = useState(true);
  var message = 'i have a job for you'

  const toggleNavbar = () => setCollapsed(!collapsed);

  return (
    <div>
      <Navbar style={{paddingTop:'2%', paddingBottom:'2%'}} color="faded" dark >
        <NavbarBrand href="/" className="mr-auto" style={{marginLeft:"40%", color:'white'}}>Copyright Zaki-Farokhi</NavbarBrand>
        <NavbarToggler  onClick={toggleNavbar} className="mr-2" />
        <Collapse isOpen={!collapsed} navbar>
          <Nav navbar>
            <NavItem>
              <NavLink href={`https://api.whatsapp.com/send?phone=6282335542575&text=%20${message}`} target='_blank'>My Whatsapp</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="https://github.com/reactstrap/reactstrap">GitHub</NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  );
}

export default Example;
