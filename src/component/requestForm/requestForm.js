import React from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Link } from 'react-router-dom'

class Example extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            email: '',
            project: '',
            description: '',
            redirect: false
        }
    }
    onchange = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    AddNewProject(){
        fetch(	"http://reduxblog.herokuapp.com/api/posts?key=requestProject",
        {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
            'Content-Type': 'application/json',
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify({
            title: 'i have project for you',
            categories: this.state.project,
            content:{
                name:this.state.name,
                email:this.state.email,
                description:this.state.description
            }
        }),
        
    })
    .then(()=>{
        this.setState({
            redirect:true
        })
    })

    }
    
    renderRedirect = () => {
        if (this.state.redirect) {
            return <link to='/' />
        }
    }

    render() {
        return (
            <Form style={{ marginLeft: '20%', marginRight: '20%', marginTop: '2%', marginBottom: '2%' }}>
                <FormGroup >
                    <Label for="exampleEmail">Email</Label>
                    <Input required type="email" name="email" id="email" className="email" placeholder="please input your email" onChange={event => this.onchange(event)} value={this.state.email} />
                </FormGroup>
                <FormGroup>
                    <Label for="exampleName">Name</Label>
                    <Input required type="text" name="name" id="name" className="name" placeholder="please input your name" onChange={event => this.onchange(event)} value={this.state.name} />
                </FormGroup>
                <FormGroup>
                    <Label for="exampleSelect">Project Categories</Label>
                    <Input required type="select" name="project" id="exampleSelect" onChange={event => this.onchange(event)} value={this.state.project} >
                        <option>select project categories</option>
                        <option>mobile project</option>
                        <option>static website project</option>
                        <option>dynamic website project</option>
                        <option>wordpress website project</option>
                    </Input>
                </FormGroup>
                <FormGroup>
                    <Label for="exampleText">Describe your project</Label>
                    <Input required type="textarea" name="description" id="exampleText" onChange={event => this.onchange(event)} value={this.state.description} />
                </FormGroup>

                <Button type='submit' onClick={this.AddNewProject.bind(this)}>Submit</Button>

            </Form>
        );

    }


}

export default Example;