import React from 'react';
import {
    Jumbotron, Button, Container,
    Row, Col, Card, CardImg, CardBody,
    CardTitle, CardSubtitle, CardHeader,
    CardFooter, CardText
} from 'reactstrap';
import { Link } from 'react-router-dom'
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";

class Example extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            projectArray: [],
            requestArray:[],
            isHiddenProject: true,
            isHiddenRequest: true
        }
    }

    componentDidMount() {
        fetch('http://reduxblog.herokuapp.com/api/posts?key=123')
        .then(Response=> Response.json())
        .then(data =>{
            this.setState(state =>({
                projectArray:[...state.projectArray, ...data],
            }))
            console.log(this.state.projectArray)
            console.log({
                success:true,
                message:'project retrieved',
                data:data
            })

        }).catch(err=>{
            console.log({
                success:false,
                message:'cannot open project',
                data:err
            })
        })
        fetch('http://reduxblog.herokuapp.com/api/posts?key=zakireduxblog')
        .then(Response=> Response.json())
        .then(data =>{
            this.setState(state =>({
                requestArray:[...state.requestArray, ...data],
            }))
            console.log(this.state.requestArray)
            console.log({
                success:true,
                message:'project request saved',
                data:data
            })
        }).catch(err=>{
            console.log({
                success:false,
                message:'cannot request project',
                data:err
            })
        })
    }

    isHiddenProject(){
        this.setState({
           isHiddenProject:!this.isHiddenProject
        })
    }
    isHiddenRequest(){
        this.setState({
            isHiddenRequest:!this.isHiddenRequest
        })
    }


    render() {
        return (
            <div>
                <Container>
                    <Row>
                        <Col xs='3' >
                            <Card>
                                <CardImg top width="100%" src={`https://ui-avatars.com/api/?name=Zaki+Farokhi`} alt="Card image cap" />
                                <CardBody>
                                    <strong> <CardTitle>Zaki Farokhi</CardTitle></strong>
                                    <strong><CardSubtitle>Mobile Developer</CardSubtitle></strong>
                                    <CardText>A Year Experience in Android Development Project </CardText>
                                    <Button onClick={this.isHiddenProject.bind(this)}>See My Project</Button>
                                    <Button onClick={this.isHiddenRequest.bind(this)}>See Project Request</Button>
                                </CardBody>
                            </Card>
                        </Col>
                        <Col xs='9'>
                            <Jumbotron style={{ background: 'black' }}>
                                <h1 className="display-3" style={{ color: 'white' }}>Hello</h1>
                                <p className="lead" style={{ color: 'white' }}>This is my portofolio, you can see my projects in here</p>
                                <hr className="my-2" />
                                <p style={{ color: 'white' }}>If you have a job for me, klik button Request below</p>
                                <p className="lead">
                                    <Link to='/request'>
                                        <Button color="primary">
                                            Request
                                        </Button>
                                    </Link>

                                </p>
                            </Jumbotron>
                        </Col>
                    </Row>
                    <Row style={{marginTop:'5%'}} hidden={this.state.isHiddenProject}>
                        <Col xs='auto' >
                            <Carousel responsive={responsive}>
                                <div style={{margin:'1%'}}>
                                    <Card>
                                        <CardHeader>Mobile Application</CardHeader>
                                        <CardBody>
                                            <CardTitle>Kalkulator Pakan by Forpate</CardTitle>
                                            <CardText>App for farmer</CardText>
                                        </CardBody>
                                    </Card>
                                </div>
                                <div style={{margin:'1%'}}>
                                    <Card>
                                        <CardHeader>Mobile Application</CardHeader>
                                        <CardBody>
                                            <CardTitle>Forpate</CardTitle>
                                            <CardText>Mobile App for Farmer to </CardText>
                                        </CardBody>
                                    </Card>
                                </div>
                                <div style={{margin:'1%'}}>
                                    <Card>
                                        <CardHeader>Web Static</CardHeader>
                                        <CardBody>
                                            <CardTitle>KampungLawasan</CardTitle>
                                            <CardText>Web Kampung Lawasan heritage and Boutique</CardText>
                                        </CardBody>
                                    </Card>
                                </div>
                                <div style={{margin:'1%'}}>
                                    <Card>
                                        <CardHeader>Web Dynamic</CardHeader>
                                        <CardBody>
                                            <CardTitle>Samasoma.com</CardTitle>
                                            <CardText>samasoma web for documentary film company</CardText>
                                        </CardBody>
                                    </Card>
                                </div>
                            </Carousel>;

                        </Col>
                    </Row>
                    <Row style={{marginTop:'5%'}} hidden={this.state.isHiddenRequest}>
                        <Col xs='auto' >
                            <Carousel responsive={responsive}>
                                {
                                    this.state.requestArray.map(request=>{
                                        if (request.content == null){
                                            request.content = ''
                                        }
                                        return(
                                            <div style={{margin:'1%'}}>
                                            <Card>
                                                <CardHeader>{request.title}</CardHeader>
                                                <CardBody>
                                                    <CardTitle>{request.categories}</CardTitle>
                                                    <CardText>{request.content.name}</CardText>
                                                    <p>{request.content.description}</p>
                                                </CardBody>
                                                <CardFooter>Footer</CardFooter>
                                            </Card>
                                        </div>
                                        )
                                    })
                                }
                            </Carousel>;

                        </Col>
                    </Row>
                </Container>

            </div>
        );
    }
};
const responsive = {
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 3,
        slidesToSlide: 3, // optional, default to 1.
    },
    tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 2,
        slidesToSlide: 2, // optional, default to 1.
    },
    mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1,
        slidesToSlide: 1, // optional, default to 1.
    },
};

export default Example;
