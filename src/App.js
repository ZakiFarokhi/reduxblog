import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Router, BrowserRouter, Link, Route, Switch } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from './component/header/header';
import Home from './component/home/home'
import Footer from './component/footer/footer';
import RequestForm from './component/requestForm/requestForm'

function App() {
  return (
    <BrowserRouter>
      <div className="App"  style={{ background: 'grey' }}>
        <main>
          <Navbar></Navbar>
          <Switch>
            <Route exact path='/' component={Home} ></Route>
            <Route exact path='/request' component={RequestForm}></Route>
          </Switch>
          <Footer style={{ background:'grey'}}></Footer>
        </main>

      </div>
    </BrowserRouter>

  );
}

export default App;
